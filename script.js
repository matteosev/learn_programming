function getComputedHeight(querySelector){
	return window.getComputedStyle(document.querySelector(querySelector)).getPropertyValue("height")
}

function toggle(id){
	// on réduit tous les composants
	for (div of document.querySelectorAll(".chapter")){
		(div.id !== id)? div.className = "chapter": "";
		div.style.height = 0;
	}
	let element = document.getElementById(id);
	element.classList.toggle('collapsed');
	if (element.classList.contains("collapsed")){
		element.style.height = height[id];
	} else{
		element.style.height = 0;
	}
}

let height = {
	"goals": getComputedHeight("#goals"),
	"ressources": getComputedHeight("#ressources"),
	"learning": getComputedHeight("#learning")
}

for (div of document.querySelectorAll(".chapter")){
	div.className = "chapter";
	div.style.height = 0;
}